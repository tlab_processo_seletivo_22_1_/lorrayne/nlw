import { createContext, useState, useEffect, ReactNode } from 'react'
import { auth, firebase } from '../services/firebase'


type User = {
    id: string;
    name: string;
    avatar: string
  }
  
type AuthContextType ={
    user: User | undefined,
    signInWhitGoogle: () => Promise<void>
}


type AuthContextProviderProps ={
    children: ReactNode;
}

export const AuthContext = createContext({} as AuthContextType);

export function AuthContextProvider(props: AuthContextProviderProps){
    const [user, setUser] = useState<User>();

    useEffect(()=> {
      const unsubscribe = auth.onAuthStateChanged(user => { //fica ouvindo o codigo e detecta se ja tem usuario
        if(user){
          const {displayName, photoURL, uid} = user
  
          if(!displayName || !photoURL){
            throw new Error('Missinf information');
          }
  
          setUser({
            id: uid,
            name: displayName,
            avatar:photoURL
          })
  
        } 
      })
      return ()=>{
        unsubscribe();
      }
    }, []) //para não perder a informação com useffect, usado dentro dos componentes do react usado para disparar uma função X sempre que algo Y acontecer
        //segundo paramentro sempre um array, para disparaar apenas uma vez ele vazio
  
    async function signInWhitGoogle(){
      const provider = new firebase.auth.GoogleAuthProvider();
  
      const result = await auth.signInWithPopup(provider);
  
      if(result.user){
        const {displayName, photoURL, uid} = result.user
  
        if(!displayName || !photoURL){
          throw new Error('Missinf information');
        }
  
        setUser({
          id: uid,
          name: displayName,
          avatar:photoURL
        })
  
      }
    }  


    return(
        <AuthContext.Provider value={{user, signInWhitGoogle}}>
            {props.children}
        </AuthContext.Provider>

        
    );
}