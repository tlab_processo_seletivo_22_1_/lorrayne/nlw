import {useHistory} from 'react-router-dom'
import ilustrationImgHome from '../assets/images/illustration.svg'
import logoImg from '../assets/images/logo.svg'
import googleIconImg from '../assets/images/google-icon.svg'

import '../styles/auth.scss'

import { Button } from '../components/Button';
import { useAuth } from '../hooks/useAuth'
import { FormEvent, useState } from 'react'
import { database } from '../services/firebase'


export function Home(){

    const history = useHistory();
    const {signInWhitGoogle, user} = useAuth()
    const [roomCode, setRoomCode ] = useState('');


    async function handleCreateRoom(){
        if(!user){
            await signInWhitGoogle();
        }
        history.push('/rooms/new')
    }

    async function handleJoineRoom (event:FormEvent) {
        event.preventDefault();

        if(roomCode.trim()== ''){
            return;
        }

        const roomRef = await database.ref(`rooms/${roomCode}`).get();
        if(!roomRef.exists()){
            alert('Room does not exist')
            return
        }

        if(roomRef.val().endedAt){
            alert('Room already closed');
            return;
        }

        history.push(`/rooms/${roomCode}`);
    }

    return(
        <div id = "page-auth">
            <aside>
                <img src = {ilustrationImgHome} alt = "ilustracao simbolizando perguntas e respostas"></img>
                <strong>Crie salas de Q&amp; A ao-vivo</strong>
                <p>Tire as dúvidas da sua audiência em tempo real</p>              
            </aside>
            <main>
                <div className = "main-content">
                    <img src ={logoImg} alt = "logo img"></img>
                    <button onClick={handleCreateRoom} className = "create-room">
                        <img src = {googleIconImg} alt = "logo google"></img>
                        Crie sua sala com o Google
                    </button>
                    <div className = "separator">
                        Ou entre em uma sala
                    </div>
                    <form onSubmit = {handleJoineRoom}>
                        <input 
                            type = "text"
                            placeholder = "Digite o código da sala"
                            onChange = {event => setRoomCode(event.target.value)}  
                            value = {roomCode}  
                                     
                        ></input>
                        <Button type = "submit">Entrar na sala</Button>
                    </form>
                </div>
            </main>
        </div>
    );
}