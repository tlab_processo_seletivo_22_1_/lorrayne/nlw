import logoImg from '../assets/images/logo.svg'
import { Button } from '../components/Button'
import { RoomCode } from '../components/RoomCode'
import '../styles/room.scss'
import { useHistory, useParams} from 'react-router-dom'
import { useAuth } from '../hooks/useAuth'
import { Question } from '../components/Question'
import { useRoom } from '../hooks/useRoom'
import deleteImg from '../assets/images/delete.svg'
import { database } from '../services/firebase'
import checkImg from '../assets/images/check.svg'
import answerImg from '../assets/images/answer.svg'


type RoomParams = {
    id: string;
}






export function AdminRoom(){
    const {user}= useAuth();
    const params = useParams<RoomParams>();
    const roomId = params.id;
    const {title, questions } = useRoom(roomId)
    const history = useHistory()

    async function handleEndRoom() {
        await database.ref(`rooms/${roomId}`).update({
            endedAt : new Date(),
        })

        history.push('/')
    }

   async function handleCheckQuestionAsAnswered(questionId: string){
            await database.ref(`rooms/${roomId}/questions/${questionId}`).update({
                isAnswered: true
            });
        
    }
    async function handleHighLightQuestion(questionId: string){
            await database.ref(`rooms/${roomId}/questions/${questionId}`).update({
               IsHighlighted: true
            });
        
    }

   async function handleDeleteQuestion(questionId: string){
    if(window.confirm('Tem certeza que deseja exlcuir essa pergunta?')){
        await database.ref(`rooms/${roomId}/questions/${questionId}`).remove();
    }
}

      return(
        <div id="page-room">
            <header>
                <div className = "content">
                    <img src = {logoImg} alt ="Letmeask"></img>
                    <div>
                        <RoomCode code = {roomId}></RoomCode>
                        <Button isOutlined onClick = {handleEndRoom}>Encerrar Sala</Button>
                    </div>
  
                </div>
            </header>
            <main className = "content" >
                <div className = "room-title">
                    <h1>Sala {title}</h1>
                    { questions.length > 0 && <span>{questions.length} pergunta(s)</span>
}
                </div>


                <div className = "question-list">
                    {questions.map(questions=> {
                        return(
                            <Question
                            key ={questions.id}
                            content = {questions.content}
                            author = {questions.author}    
                            isAnswered = {questions.isAnswered}
                            IsHighlighted = {questions.IsHighlighted}      
                           >
                            {
                            !questions.isAnswered && (
                                <>
                                    <button
                                        type = "button"
                                        onClick = {() => handleCheckQuestionAsAnswered(questions.id)}
                                    >
                                        <img src ={checkImg} alt = "Marcar pergunta como respondida"></img>
                                    </button>
                                    <button
                                        type = "button"
                                        onClick = {() => handleHighLightQuestion(questions.id)}
                                    >
                                        <img src ={answerImg} alt = "Dar ênfase a pergunta"></img>
                                    </button>
                                </>
                            )}
                            <button
                                type = "button"
                                onClick = {() => handleDeleteQuestion(questions.id)}
                            >
                                   <img src ={deleteImg} alt = "Remover pergunta"></img>
                            </button>
                           </Question> 
                        )
                    })}
                </div>
            </main>
        </div>
    )
}