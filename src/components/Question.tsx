import { ReactNode } from 'react';
import cx from 'classnames'
import '../styles/question.scss'


type QuestionProps ={
    content: string;
    author: {
        name: string;
        avatar: string;
    };

    children?: ReactNode,
    isAnswered?: boolean,
    IsHighlighted?: boolean;

}




export function Question ({
    content,
    author,
    children,
    isAnswered = false,
    IsHighlighted = false,
    }:QuestionProps){
    return (
     <div 
        className ={cx 
        ('question', 
        {answered: isAnswered},
        {highlighted:IsHighlighted && !isAnswered},

        )}>
         <p>{content}</p>
         <footer>
             <div className = "user-info">
                 <img src = {author.avatar} alt = {author.name} referrerPolicy="no-referrer"></img>
                <span>{author.name}</span>
             </div>
             <div>
                {children}
             </div>
         </footer>
     </div>
    )
}