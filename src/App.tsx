import { Home } from "./pages/home";
import {Route, BrowserRouter, Switch } from 'react-router-dom'
import { NewRoom } from "./pages/NewRoom";
import { AuthContextProvider } from './contexts/AuthContext'
import { Room } from "./pages/Room";
import { AdminRoom } from "./pages/AdminRoom";



function App() {

  return (
  <BrowserRouter>
    <AuthContextProvider>
      <Switch>
        <Route path="/" exact = {true} component = {Home}></Route>
        <Route path="/rooms/new" exact component = {NewRoom}></Route>
        <Route path="/rooms/:id" component = {Room}></Route>
        <Route path="/admin/rooms/:id" component = {AdminRoom}></Route>

      </Switch>
    </AuthContextProvider>
  </BrowserRouter>
  );
}

export default App;


//HTML dentro do js é jsx
